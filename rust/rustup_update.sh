#!/bin/bash -l

CHANNEL=${RUST_VERSION:-stable}

rustup update $CHANNEL

rustup -V
rustc -V
cargo -V

exec "$@"
