FROM rustdocker/rustup
LABEL maintainer="Cyril Plisko <cyril.plisko@mountall.com>"
ARG BUILD_DATE
ARG RUST_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="rustdocker-rust" \
      org.label-schema.description="Rust development environment" \
      org.label-schema.url="https://gitlab.com/imp/rustdocker" \
      org.label-schema.schema-version="1.0"
ENV RUST_VERSION=${RUST_VERSION:-stable}
ADD rustup_update.sh /root/rustup_update.sh
RUN . ~/.cargo/env \
    && rustup install $RUST_VERSION \
    && rustup default $RUST_VERSION \
    && rustup component add rustfmt \
    && rustup component add clippy
ENTRYPOINT ["/root/rustup_update.sh"]
CMD ["/bin/bash"]
